/*
	CHILD PROCESS
		by Daniel Scutt
	1) Tests connections.js as a child.
	2) Compatible thru V0.03
*/


/* Required Functions */


/* Imports */
// Connections class
var conn_class = require('./connections.js');


/* Variables */
// Local data
var local_ip = '127.0.0.1';
var local_port = 5001;
// Child Connection Names
var conn1 = "Listener";


/* Objects */
var connection = new conn_class.Connections("Child");


/* Logic */
connection.LoopLogic();
connection.SetupConnectionsHandler();
connection.SetupChildProcessListener();


connection.ConnectionsHandler.on('receive',(args) => {
	try {
		var c = args[0];
		var t = args[1];
		var m = args[2];
		var r = false;
		if (t == "socket" && args.length == 4) r = args[3];
		
		// Parse Message
		var reply = false;
		var msg = connection.parsemessage(m);
		var title = false;
		if ('title' in msg) title = msg['title'];
		
		// Look For Arguments
		if (title == "TESTCONN") {
			reply = connection.formatmessage({'title':'CONNSAT','reply':'test sat'});
		} else if (title == "CONNSAT" && 'reply' in msg) {
			connection.print("From "+c+" - "+msg['reply']);
		}
		
		// Send reply if required
		if (reply && t == "socket") {
			// Reply via socket
			connection.SendMessage(c,reply,r.address,r.port);
		} else if (reply && t == "child") {
			// Reply via relationship
			connection.SendMessage(c,reply);
		} else if (reply && t == "parent") {
			// Reply to parent
			connection.SendMessage("parent",reply);
		}
	} catch(err) {
		connection.print("ConnectionsHandler failed on error: "+err)
	}
});











